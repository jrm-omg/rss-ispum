<?php

/**
 * RSS Ipsum v0.1
 */

$cache = isset($_GET['cache']) ? true : false;

$allFeeds = [
  "https://theconversation.com/fr/articles.rss",
  "http://feeds.feedburner.com/theatlantic/infocus",
  "https://theintercept.com/feed/?lang=en",
  "https://www.courrierinternational.com/feed/category/6260/rss.xml",
  "https://www.monde-diplomatique.fr/recents.xml",
  "https://torrentfreak.com/feed/",
  "https://www.lemonde.fr/rss/plus-lus.xml",
  "https://www.pixeldetracking.com/fr/.rss",
  "https://www.liberation.fr/arc/outboundfeeds/rss/?outputType=xml",
  "https://www.nextinpact.com/rss/news.xml",
  "https://www.theguardian.com/science/rss",
  "https://www.theguardian.com/international/rss",
  "https://www.lefigaro.fr/rss/figaro_international.xml",
  "https://api.blast-info.fr/rss.xml",
  "https://news.ycombinator.com/rss",
];

// feeds filtering
$feeds = [];
$qs = array_keys($_GET);
if (!empty($qs)) {
  $ids = explode(',', $qs[0]);
  foreach ($ids as $id) {
    $k = (int)$id;
    if (array_key_exists($k, $allFeeds)) {
      $feeds[] = $allFeeds[$k];
    }
  }
}
else {
  $feeds = $allFeeds;
}
if(empty($feeds)) $feeds = $allFeeds;

// choose one feed
$feedURL = $feeds[array_rand($feeds)];

if (!$cache) {
  // download that feed
  // using cURL (as some servers only accept cURL for remote content)
  $curl = curl_init();
  $curl_options = [
    CURLOPT_URL => $feedURL,
    CURLOPT_USERAGENT => "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/100.0",
    CURLOPT_HEADER => false,    
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_FOLLOWLOCATION => true
  ];
  curl_setopt_array($curl, $curl_options);
  $content = curl_exec($curl);
  curl_close($curl);
}

// online or not ?
if($cache OR strlen($content) === 0) {

  // offline
  $json = file_get_contents('rss-ipsum.cache.json');

} else {

  // online
  $xmlOb = new SimpleXMLElement($content, LIBXML_NOCDATA);
  foreach ($xmlOb->channel->item as $item_v) {
    $entry = $item_v;
    $entry->content = $item_v->children("content", true);
    $articles[] = $entry;
  }
  $randKeys = array_rand($articles, 5); // pick 5 random articles
  $someArticles = [];
  foreach ($randKeys as $key) {
    $title = (string) strip_tags($articles[$key]->title);
    $description = (string) strip_tags($articles[$key]->description, "<img><figure><figcaption><p><a><i><em><b><strong><h1><h2><h3><h4><h5><h6>");
    $link = (string) strip_tags($articles[$key]->link);
    preg_match('/^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^:\/?\n]+)/', $link, $domain); // domain
    $domain = $domain[1];
    $dateObj = new DateTime($articles[$key]->pubDate);
    $someArticles[] = [
      "title" => $title,
      "content" => $description,
      "domain" => $domain,
      "link" => $link,
      "dateTS" => $dateObj->getTimestamp(),
      "dateSTR" => $dateObj->format('Y-m-d H:i:s'),
    ];
  }
  $json = json_encode($someArticles);
  @file_put_contents('rss-ipsum.cache.json', $json);
}

header('Content-Type: application/json; charset=utf-8');
echo $json;