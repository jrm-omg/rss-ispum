# RSS Ipsum

Use real RSS news, as JSON ipsum.

[Demo](https://humanize.me/rss-ipsum/demo.html)

## Usage

Pick some articles :

- `rss-ipsum.php` : from any feed
- `rss-ipsum.php?1,4,7` : from one of these feeds (ids 1, 4, 7)
- `rss-ipsum.php?0` : from one feed (id 0)
- `rss-ipsum.php?cache` : from the cache

## License

GNU GPLv3. See `LICENSE` for more information.